package stash.no.push.to.master;

import com.atlassian.stash.hook.*;
import com.atlassian.stash.hook.repository.*;
import com.atlassian.stash.repository.*;
import java.util.Collection;

public class NoPushToMasterHook implements PreReceiveRepositoryHook {

    /**
     * Disables direct push to master branch
     */
    @Override
    public boolean onReceive(RepositoryHookContext context, Collection<RefChange> refChanges, HookResponse hookResponse) {
        for (RefChange refChange : refChanges) {
            if (refChange.getRefId().equals("refs/heads/master") && refChange.getType() == RefChangeType.UPDATE) {
                hookResponse.err().println("The master branch ('" + refChange.getRefId() + "') cannot be updated.");
                hookResponse.err().println(" ______       _ _                                             _ _ ");
                hookResponse.err().println("(_____ \\     | | |                                    _      | | |");
                hookResponse.err().println(" _____) )   _| | |    ____ ____ ____ _   _  ____  ___| |_    | | |");
                hookResponse.err().println("|  ____/ | | | | |   / ___) _  ) _  | | | |/ _  )/___)  _)   |_|_|");
                hookResponse.err().println("| |    | |_| | | |  | |  ( (/ / | | | |_| ( (/ /|___ | |__    _ _ ");
                hookResponse.err().println("|_|     \\____|_|_|  |_|   \\____)_|| |\\____|\\____|___/ \\___)  |_|_|");
                hookResponse.err().println("                                  |_|                             ");

                return false;
            }
        }
        return true;
    }
}