# stash-no-push-to-master

A simple repository hook to protect master branch from direct push.

In other words, it's like "Please pull request if you'd like to make change on master branch".

## How to build

- Follow [Install the Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
- Do git clone, move to the repo, run below command and then check stash-no-push-to-master-*.jar under target directory.

```
> atlas-mvn package
...
...
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 8.309 s
[INFO] Finished at: 2015-06-17T16:04:00+09:00
[INFO] Final Memory: 36M/248M
[INFO] ------------------------------------------------------------------------
```

## How it works

- git push origin develop: Will be accepted.
- git push origin --tags: Will be accepted.
- Merging pull-request from develop to master: Will be merged.
- git push origin master: Will be rejected with below message.

```
remote: The master branch ('refs/heads/master') cannot be updated.
remote:  ______       _ _                                             _ _
remote: (_____ \     | | |                                    _      | | |
remote:  _____) )   _| | |    ____ ____ ____ _   _  ____  ___| |_    | | |
remote: |  ____/ | | | | |   / ___) _  ) _  | | | |/ _  )/___)  _)   |_|_|
remote: | |    | |_| | | |  | |  ( (/ / | | | |_| ( (/ /|___ | |__    _ _
remote: |_|     \____|_|_|  |_|   \____)_|| |\____|\____|___/ \___)  |_|_|
remote:                                   |_|
```

## Screenshots

### plugin-administration-page
![plugin-administration-page.png](./src/main/resources/images/plugin-administration-page.png)

### repo-settings-page
![repo-settings-page.png](./src/main/resources/images/repo-settings-page.png)


_Forked from https://bitbucket.org/atlassian/stash-example-pre-receive-no-config_
